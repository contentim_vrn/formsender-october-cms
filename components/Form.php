<?php namespace Contentim\FormSender\Components;

use Cms\Classes\ComponentBase;
use ApplicationException;
use Contentim\FormSender\Models\Message;
use Contentim\FormSender\Models\Spam;
use Carbon\Carbon;

use Mail;
use Url;
use Input;
use Request;
use Response;
use Validator;
use Redirect;
use ValidationException;

class Form extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'FormSender Demka',
            'description' => 'Its contact form sender to email'
        ];
    }

    public function defineProperties()
    {
        return [
            'max' => [
                'description'       => 'The most amount of todo items allowed',
                'title'             => 'Max items',
                'default'           => 10,
                'type'              => 'string',
                'validationPattern' => '^[0-9]+$',
                'validationMessage' => 'The Max Items value is required and should be integer.'
            ]
        ];
    }

    public function onSend()
    {
        $data = [
            'name' => Input::get('name'),
            'email' => Input::get('email'),
            'subject' => Input::get('subject'),
            'msg' => post('msg'),
            'ip' => Request::ip()
        ];

        $validator = Validator::make(
            $data,
            [
                'email' => 'required|email',
                'subject' => 'required|min:5',
                'msg' => 'required',
            ]
        );

        if ($validator->fails()) {

            return Redirect::back()->withErrors($validator);

        } else {

            $data_table = new Message();

            /* временная мера от спама */
            if (strlen(Input::get('name')) > 0 ){
                $data_table->is_spam = 1;
            } else {
                $data_table->is_spam = 0;
            }

            $data_table->is_new = 1;
            $data_table->email = Input::get('email');
            $data_table->subject = Input::get('subject');
            $data_table->message = Input::get('msg');
            $data_table->remote_ip = Request::ip();
            $data_table->created_at = Carbon::now();
            $data_table->save();



            /* работает, раскомментировать
            */
            /*
            Mail::send('contentim.form::mail.test', $data, function ($message) {

                $message->to('contentim@yandex.ru', Input::get('email'))
                    ->subject(Input::get('subject'))
                    ->replyTo('mail@ibuildrussia.ru', 'Заявка с сайта');
                $message->from('mail@ibuildrussia.ru', 'Заявка с сайта');

            });
            */

        }
    }
}
