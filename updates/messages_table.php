<?
    namespace Contentim\FormSender\Updates;

    use Shema;
    use October\Rain\Database\Updates\Migration;

    class Messages_table extends Migration {
        public function up(){
            Shema::create('contentim_formsender_messages', function ($table){
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->tinyInteger('is_new',1)->nullable();
                $table->tinyInteger('is_spam',0)->nullable();
                $table->tinyInteger('important',0)->nullable();
                $table->text('email')->nullable();
                $table->text('subject')->nullable();
                $table->text('message')->nullable();
                $table->string('remote_ip', 100)->nullable();
                $table->timestamp();
            });
        }

        public function down(){
            Schema::dropIfExists('contentim_formsender_messages');
        }
    }