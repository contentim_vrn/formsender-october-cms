<?php

namespace Contentim\FormSender\Classes;

use Contentim\FormSender\Models\Message;
use Contentim\FormSender\Models\Spam;

use App;
use Carbon\Carbon;
use Backend;

class LatestThemeMessages{

    public function index($data)
    {

        switch ($data){
            case 'message':
                $object = new Message();
                break;
            case 'spam':
                $object = new Spam();
                break;
        }

        $value = $object::latestMessages();
        if ( !empty($value->is_new) && $value->is_new > 0 ) {

            if(mb_strlen($value->subject) > 25) {
                $subject = mb_substr(strip_tags($value->subject), 0, 25).'...';
            } else {
                $subject = $value->subject;
            }

            Carbon::setLocale( App::getLocale() );
            $date_created = Carbon::createFromFormat( 'Y-m-d H:i:s', $value->created_at )->diffForHumans();

            $str = "<p class='latest_theme'><a href='".Backend::url('contentim/formsender/'.$data.'/preview')."/".$value->id."'><i class='icon-envelope'></i>".$subject."</a></p>";
            $str .= "<p class='description'>".$date_created."</p>";
        } else {
            $str = "<p class='latest_theme'><i class='icon-envelope'></i>".e(trans('contentim.formsender::lang.controller.scoreboard.all_read'))."</p>";
        }

        return $str;
    }

}