<?php

namespace Contentim\FormSender\Classes;

use Contentim\FormSender\Models\Message;
use Contentim\FormSender\Models\Spam;

class UnreadMessages {

    public static function getTotalMessage() {
        $unread = Message::where('is_new', 1)
            ->where('is_spam', 0)
            ->count();
        return ($unread > 0) ? $unread : null;
    }

    public static function getTotalSpam() {
        $unread = Spam::where('is_new', 1)
            ->where('is_spam', 1)
            ->count();
        return ($unread > 0) ? $unread : null;
    }

}

?>