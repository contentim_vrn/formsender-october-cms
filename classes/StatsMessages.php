<?php
    namespace Contentim\FormSender\Classes;

    use Contentim\FormSender\Models\Message;
    use Contentim\FormSender\Models\Spam;

    use App;
    use Carbon\Carbon;

    class StatsMessages{

        public function index($object, $event){

            switch ($object){
                case 'message':
                    $object = new Message();
                    break;
                case 'spam':
                    $object = new Spam();
                    break;
            }

            switch($event){

                case 'all_count':
                    return $object::count();
                    break;
                case 'new_count':
                    return $object::isNew()->count();
                    break;
                case 'read_count':
                    return $object::isRead()->count();
                    break;
                case 'important_count':
                    return $object::isImportant()->count();
                    break;
                case 'latest_message_date':

                    $value = $object::orderBy('created_at', 'DESC')->first();
                    if ( !empty( $value->created_at) ) {
                        Carbon::setLocale( App::getLocale() );
                        return Carbon::createFromFormat( 'Y-m-d H:i:s', $value->created_at )->diffForHumans();
                    }

                    return NULL;
                    break;
                case 'latest_theme':

                    $value = $object::orderBy('created_at', 'DESC')->first();
                    if ( !empty( $value->subject) ) {
                        return $value->subject;
                    }

                    return NULL;
                    break;
            }
        }

    }