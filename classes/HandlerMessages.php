<?php

namespace Contentim\FormSender\Classes;

use Contentim\FormSender\Models\Message;
use Contentim\FormSender\Models\Spam;

use Flash;
use App;
use Carbon\Carbon;
use Backend;
use ApplicationException;
use Mail;
use Url;
use Input;
use Request;
use Response;
use Validator;
use Redirect;
use ValidationException;

class HandlerMessages {

    public function index($object) {

       /* if (!$this->user->hasAccess('contentim.formsender.access_messages')) {
            Flash::error( e(trans('contentim.formsender::lang.controllers.index.unauthorized')) );
            return;
        }*/

       switch ($object){
           case 'message':
               $object = new Message();
               Flash::success( e(trans('contentim.formsender::lang.controller.scoreboard.mark_read_success')) );
               break;
           case 'spam':
               $object = new Spam();
               Flash::success( e(trans('contentim.formsender::lang.controller.scoreboard.mark_read_success')) );
               break;
       }

        $checkedIds = post('checked');

        if ( ($checkedIds) && is_array($checkedIds) && count($checkedIds) ) {

            foreach ($checkedIds as $item) {
                if (!$message = $object::find($item)) {
                    continue;
                }

                switch (post('handler')){
                    case 'read':
                        $message->is_new = 0;
                        Flash::success( e(trans('contentim.formsender::lang.controller.scoreboard.mark_read_success')) );
                        break;
                    case 'unread':
                        $message->is_new = 1;
                        Flash::success( e(trans('contentim.formsender::lang.controller.scoreboard.mark_unread_success')) );
                        break;
                    case 'important':
                        $message->important = 1;
                        Flash::success( e(trans('contentim.formsender::lang.controller.scoreboard.mark_important_success')) );
                        break;
                    case 'unimportant':
                        $message->important = 0;
                        Flash::success( e(trans('contentim.formsender::lang.controller.scoreboard.mark_unimportant_success')) );
                        break;
                    case 'add_spam':
                        $message->is_spam = 1;
                        Flash::success( e(trans('contentim.formsender::lang.controller.scoreboard.mark_add_spam_success')) );
                        break;
                    case 'remove_spamlist':
                        $message->is_spam = 0;
                        Flash::success( e(trans('contentim.formsender::lang.controller.scoreboard.mark_add_spam_success')) );
                        break;
                    case 'delete':
                        $message->delete();
                        Flash::success( e(trans('contentim.formsender::lang.controller.scoreboard.mark_delete_success')) );
                        break;
                }
                $message->save();

            }

//                return $this->listRefresh();
            return Redirect::refresh();

        }
    }


}

?>