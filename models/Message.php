<?php

    namespace Contentim\FormSender\Models;

    use Str;
    use Model;
    use URL;
    use October\Rain\Router\Helper as RouterHelper;
    use Cms\Classes\Page as CmsPage;
    use Cms\Classes\Theme;
    use Log;
    use Validator;
    use Mail;
    use Request;
    use Carbon\Carbon;
    use View;
    use App;
    use System\Models\MailTemplate;
    use System\Classes\MailManager;
    

    class Message extends Model
    {
//        use \October\Rain\Database\Traits\SoftDelete;

        protected $table = 'contentim_formsender_messages';

        /**
         * Scope new messages only
         */
        public function scopeIsNew($query)
        {
            return $query->where([
                ['is_new', '=', 1],
                ['is_spam', '=', 0]
            ]);
        }

        /**
         * Scope new messages only
         */
        public function scopeIsRead($query)
        {
            return $query->where([
                ['is_new', '=', 0],
                ['is_spam', '=', 0],
            ]);
        }

        /**
         * Scope new messages only
         */
        public function scopeIsImportant($query)
        {
            return $query->where([
                ['important', '=', 1],
                ['is_spam', '=', 0],
            ]);
        }

        public function scopeLatestMessages($query)
        {
            return $query->where([
                    ['is_new', '=', 1],
                    ['is_spam', '=', 0],
                ])->orderBy('created_at', 'DESC')
                ->first();
        }

        /**
         * Scope new messages only
         */
        public function scopeAllMessage($query)
        {
            return $query->where('is_spam', '=', 0);
        }

    }