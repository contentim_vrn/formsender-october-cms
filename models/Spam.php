<?php
namespace Contentim\FormSender\Models;

use DB;
use Str;
use Model;
use URL;
use October\Rain\Router\Helper as RouterHelper;
use Cms\Classes\Page as CmsPage;
use Cms\Classes\Theme;
use Log;
use Validator;
use Mail;
use Request;
use Carbon\Carbon;
use View;
use App;
use System\Models\MailTemplate;
use System\Classes\MailManager;

use Contentim\FormSender\Models\Message;


class Spam extends Model
{
    protected $table = 'contentim_formsender_messages';

    public function __construct()
    {

    }

    public function scopeIsNew($query)
    {
        return $query->where([
            ['is_new', '=', 1],
            ['is_spam', '=', 1],
        ]);
    }

    public function scopeIsRead($query)
    {
        return $query->where([
            ['is_new', '=', 0],
            ['is_spam', '=', 1],
        ]);
    }

    public function scopeIsImportant($query)
    {
        return $query->where([
            ['important', '=', 1],
            ['is_spam', '=', 1],
        ]);
    }

    public function scopeLatestMessages($query)
    {
        return $query->where([
            ['is_new', '=', 1],
            ['is_spam', '=', 1],
        ])->orderBy('created_at', 'DESC')
            ->first();
    }

    public function addBlacklist($query){

    }

}
