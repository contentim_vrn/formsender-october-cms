<?php

    namespace Contentim\FormSender\Controllers;

    use BackendMenu;
    use Backend\Classes\Controller;
    use Cms\Classes\ComponentBase;

    use Contentim\FormSender\Models\Spam;
    use Contentim\FormSender\Classes\HandlerMessages;
    use Contentim\FormSender\Classes\StatsMessages;
    use Contentim\FormSender\Classes\LatestThemeMessages;

    use Flash;
    use App;
    use Carbon\Carbon;
    use Backend;
    use ApplicationException;
    use Mail;
    use Url;
    use Input;
    use Request;
    use Response;
    use Validator;
    use Redirect;
    use ValidationException;

    class Spams extends Controller
    {

        public $implement = [
            'Backend.Behaviors.ListController',
        ];

        public $listConfig = 'config_list.yaml';

        public function __construct()
        {
            parent::__construct();
            BackendMenu::setContext('Contentim.FormSender', 'formsender', 'spams');
        }

        public function index()
        {
            $this->pageTitle = e(trans('contentim.formsender::lang.controller.scoreboard.spam_title'));
            $this->asExtension('ListController')->index();
            $this->addCss('/plugins/contentim/formsender/assets/css/formsender.css');
        }

        public function listExtendQuery($query)
        {
            $query->where('is_spam', 1)->get();
        }

        /**
         *
         * Handler messages
         *
         */
        public function onAction()
        {
            $object = new HandlerMessages();
            if (!$this->user->hasAccess('contentim.formsender.access_messages')) {
                Flash::error( e(trans('contentim.formsender::lang.controllers.index.unauthorized')) );
                return;
            }
            return $object->index('spam');
        }

        /**
         *
         * Generate messages statsitics
         *
         */
        public function getMessagesStats($obj, $event)
        {
            $object = new StatsMessages();
            return $object->index($obj, $event);
        }

        /**
         *
         * Generate messages statsitics
         *
         */
        public function getMsgLatest()
        {
            $object = new LatestThemeMessages();
            return $object->index('spam');
        }

        public function preview($id)
        {
            $spam_message = Spam::find($id);
            $this->vars['message'] = $spam_message;
            $spam_message->is_new = 0;
            $spam_message->save();
        }
    }





























