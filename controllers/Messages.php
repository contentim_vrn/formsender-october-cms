<?php

    namespace Contentim\FormSender\Controllers;

    use BackendMenu;
    use Backend\Classes\Controller;
    use Cms\Classes\ComponentBase;
//    use Contentim\FormSender\Models\Settings;

    use Contentim\FormSender\Models\Message;
    use Contentim\FormSender\Classes\HandlerMessages;
    use Contentim\FormSender\Classes\StatsMessages;
    use Contentim\FormSender\Classes\LatestThemeMessages;

    use Flash;
    use App;
    use Carbon\Carbon;
    use Backend;
    use ApplicationException;
    use Mail;
    use Url;
    use Input;
    use Request;
    use Response;
    use Validator;
    use Redirect;
    use ValidationException;

    class Messages extends Controller
    {

        public $implement = [
            'Backend.Behaviors.ListController',
            'Backend.Behaviors.ImportExportController',
        ];

        public $listConfig = 'config_list.yaml';

        public $importExportConfig = 'config_export.yaml';

        public function __construct()
        {
            parent::__construct();
            BackendMenu::setContext('Contentim.FormSender', 'formsender', 'messages');
        }

        public function index()
        {
            $this->pageTitle = e(trans('contentim.formsender::lang.controller.scoreboard.message_title'));
            $this->asExtension('ListController')->index();
            $this->addCss('/plugins/contentim/formsender/assets/css/formsender.css');

        }

        public function listExtendQuery($query)
        {
            $query->where('is_spam', 0)->get();
        }

        public function onAction()
        {
            $object = new HandlerMessages();
            if (!$this->user->hasAccess('contentim.formsender.access_messages')) {
                Flash::error( e(trans('contentim.formsender::lang.controllers.index.unauthorized')) );
                return;
            }
            return $object->index('message');
        }

        /**
         *
         * Generate messages statsitics
         *
         */
        public function getMessagesStats($obj, $event)
        {
            $object = new StatsMessages();
            return $object->index($obj, $event);
        }

        /**
         *
         * Generate title latest new messages
         *
         */
        public function getMsgLatest()
        {
            $object = new LatestThemeMessages();
            return $object->index('message');
        }

        public function preview($id)
        {
            $message = Message::find($id);
            $this->vars['message'] = $message;
            $message->is_new = 0;
            $message->save();
        }
    }