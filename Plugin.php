<?php

    namespace Contentim\FormSender;
    use \Illuminate\Support\Facades\Event;

    use Backend, Lang, Validator;
    use System\Classes\PluginBase;
    use System\Classes\SettingsManager;

    use Contentim\FormSender\Classes\UnreadMessages;

    use Contentim\FormSender\Controllers\Messages as FormSenderController;

    class Plugin extends PluginBase
    {
        public function pluginDetails()
        {
            return [
                'name'        => 'FormSender Demo',
                'description' => 'Lorem ipsum dolor it.',
                'author'      => 'Ivan Goncharenko',
                'icon'        => 'icon-leaf'
            ];
        }

        /*
         *
         * ыфвфывфыв фыв фыв
         *
         * */

        public function registerNavigation() {
//            if(Settings::get('global_hide_button', false)) { return; }
            return [
                'formsender' => [
                    'label'       => 'Сообщения',
                    'icon'        => 'icon-envelope-o',
                    //'iconSvg'     => 'plugins/martin/forms/assets/imgs/icon.svg',
                    'url'         => Backend::url('contentim/formsender/messages'),
//                    'url'         => BackendHelpers::getBackendURL(['martin.forms.access_records' => 'contentim/formsender/messages', 'martin.forms.access_exports' => 'contentim/formsender/exports'], 'martin.forms.access_records'),
//                    'permissions' => ['martin.forms.*'],
                    'sideMenu' => [
                        'messages' => [
                            'label'        => 'contentim.formsender::lang.controller.scoreboard.messages_btn',
                            'icon'         => 'icon-envelope-o',
                            'url'          => Backend::url('contentim/formsender/messages'),
                            'permissions'  => ['contentim.formsender.access_messages'],
                            'counter'      => UnreadMessages::getTotalMessage(),
                            'counterLabel' => 'Un-Read Messages'
                        ],
                        'spams' => [
                            'label'       => 'contentim.formsender::lang.controller.scoreboard.spam_btn',
                            'icon'        => 'icon-bug',
                            'url'         => Backend::url('contentim/formsender/spams'),
                            'permissions' => ['contentim.formsender.access_spams'],
                            'counter'      => UnreadMessages::getTotalSpam(), //UnreadMessages::getTotalSpam(),
                        ],

                        /*
                        'exports' => [
                            'label'       => 'contentim.formsender::lang.controller.scoreboard.export_btn',
                            'icon'        => 'icon-download',
                            'url'         => Backend::url('contentim/formsender/messages/export'),
                            'permissions' => ['contentim.formsender.access_exports']
                        ],
                        'setting' => [
                            'label'       => 'contentim.formsender::lang.controller.scoreboard.settings_btn',
                            'icon'        => 'icon-cogs',
                            'url'         => Backend::url('contentim/formsender/setting'),
                            'permissions' => ['contentim.formsender.access_exports']
                        ],
                        */
                    ]
                ]
            ];
        }

        public function registerComponents()
        {
            return [
                '\Contentim\FormSender\Components\Form' => 'FormSender'
            ];
        }

        public function registerPageSnippets()
        {
            return [
                '\Contentim\FormSender\Components\Form' => 'FormSender',
            ];
        }


        public function registerSettings()
        {
        }

        /**
         *	Custom list types
         */
        public function registerListColumnTypes()
        {

            return [
                'strong' => function($value) {
                        return '<strong>'. $value . '</strong>';
                    },
                'text_preview' => function($value) {
                        $content = mb_substr(strip_tags($value), 0, 50);
                        if(mb_strlen($content) > 50) {
                            return ($content . '...');
                        } else {
                            return $content;
                        }
                    },
                'array_preview' => function($value) {
                        $content = mb_substr(strip_tags( implode(' --- ', $value) ), 0, 150);
                        if(mb_strlen($content) > 150) {
                            return ($content . '...');
                        } else {
                            return $content;
                        }
                    },
                'switch_icon_star' => function($value) {
                        return '<div class="text-center"><span class="'. ($value==1 ? 'oc-icon-circle text-success' : 'text-muted oc-icon-circle text-draft') .'"></span></div>'; // ($value==1 ? e(trans('contentim.formsender::lang.models.message.columns.new')) : e(trans('contentim.formsender::lang.models.message.columns.read')) )
                    },
                'switch_important' => function($value) {
                    $stat = null;

//                    print_r($value);

                        if($value){
                            $stat = 'important-active';
                        }

                        return '<div class="text-center"><a class="icon-star '.$stat.'" href="javascript:;"></a></div>';
                    },
                'switch_extended' => function($value) {
                        if($value){
                            return '<span class="list-badge badge-success"><span class="icon-check"></span></span>';
                        } else {
                            return '<span class="list-badge badge-danger"><span class="icon-minus"></span></span>';
                        }
                    },
                'attached_images_count' => function($value){
                        return (count($value) ? count($value) : NULL);
                    },
                'image_preview' => function($value) {
                    $width = Settings::get('records_list_preview_width') ? Settings::get('records_list_preview_width') : 50;
                    $height = Settings::get('records_list_preview_height') ? Settings::get('records_list_preview_height') : 50;

                    if($value){
                        return "<img src='".$value->getThumb($width, $height)."' style='width: auto; height: auto; max-width: ".$width."px; max-height: ".$height."px'>";
                    }
                },
            ];

        }
    }
